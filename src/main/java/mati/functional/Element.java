package mati.functional;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by mati on 24.04.15.
 */
public class Element {

	/**
	 *
	 * @param c method to call on the T element
	 * @param arg argument to pass to the c method
	 * @param <T> Type of the element on which the method c will be called
	 * @param <U> Type of the parameter of c method
	 * @return a consumer which accepts a T element and invokes the c method on it
	 */
	public static <T, U> Consumer<T> consume(BiConsumer<? super T, U> c, U arg) {
		return (T element) -> c.accept(element, arg);
	}

	/**
	 *
	 *
	 *
	 * @param c method to call on the T element
	 * @param arg1 first argument to be passed to the c method
	 * @param arg2 second argument to be passed to the c method
	 * @param <T> Type of the element on which the method c will be called
	 * @param <U> Type of the first argument of the c method
	 * @param <V> Type of second argument of the c method
	 * @return a consumer which accepts a T element and invokes the c method on it
	 */
	public static <T, U, V> Consumer<T> consume(ThreeConsumer<? super T, U, V> c, U arg1, V arg2) {
		return (T element) -> c.accept(element, arg1, arg2);
	}

	/**
	 *
	 * @param f
	 * @param arg1
	 * @param <T>
	 * @param <U>
	 * @param <R>
	 * @return
	 */
	public static <T, U, R> Function<T, R> apply(BiFunction<? super T, U, R> f, U arg1) {
		return element -> f.apply(element, arg1);
	}

	/**
	 *
	 * @param f
	 * @param arg1
	 * @param arg2
	 * @param <T>
	 * @param <U>
	 * @param <V>
	 * @param <R>
	 * @return
	 */
	public static <T, U, V, R> Function<T, R> apply(TriFunction<? super T, U, V, R> f, U arg1, V arg2) {
		return element -> f.apply(element, arg1, arg2);
	}
}
