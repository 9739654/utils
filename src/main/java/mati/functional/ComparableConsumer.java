package mati.functional;

import java.util.Comparator;
import java.util.function.Consumer;

public interface ComparableConsumer<T> extends Consumer<T>, Comparable<T> {

}
