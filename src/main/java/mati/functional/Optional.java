package mati.functional;

import java.util.function.Consumer;

public class Optional<T> {

	private T value;

	private Optional() {
		this(null);
	}

	private Optional(T value) {
		this.value = value;
	}

	public final T get() {
		return value;
	}

	public final Optional<T> set(T value) {
		this.value = value;
		return this;
	}

	public final boolean isSet() {
		return value != null;
	}

	public final boolean notSet() {
		return value == null;
	}

	public Optional<T> ifSet(Consumer<T> consumer) {
		if (value != null) {
			consumer.accept(value);
		}
		return this;
	}

	public Optional<T> orElse(Consumer consumer) {
		if (value == null) {
			consumer.accept(null);
		}
		return this;
	}

	public static final <S> Optional<S> of(S element) {
		return new Optional<>(element);
	}
}
