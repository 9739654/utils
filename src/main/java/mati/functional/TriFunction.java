package mati.functional;

/**
 * Created by mati on 24.04.15.
 */
@FunctionalInterface
public interface TriFunction<T, U, V, R> {
	R apply(T t, U u, V v);
}
