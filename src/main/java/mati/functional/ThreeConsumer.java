package mati.functional;

import java.util.function.Consumer;

/**
 * Created by mati on 24.04.15.
 */
@FunctionalInterface
public interface ThreeConsumer<T, U, V> {
	void accept(T t, U u, V v);
}
