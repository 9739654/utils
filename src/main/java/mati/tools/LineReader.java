package mati.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class LineReader<T, U extends LineReader<T, U>> extends Converter<InputStream, T, U> {

	private BufferedReader br;
	private int lineNumber;
	private boolean stop;

	/**
	 * Should instatiate result here.
	 */
	protected void onInit() {

	}

	@Override
	protected final U onProcess() {
		br = new BufferedReader(new InputStreamReader(source));
		onInit();
		String line;
		try {
			while ((line = br.readLine()) != null && !stop) {
				lineNumber += 1;
				onReadLine(line);
				if (error != null) {
					log.severe(error);
					return getMe();
				}
			}
		} catch (IOException e) {
			error = e.getLocalizedMessage();
			log.severe(error);
		}
		return getMe();
	}

	protected final int getLineNumber() {
		return lineNumber;
	}

	/**
	 * Called on line read.
	 * @param line last read line
	 */
	protected void onReadLine(String line) {

	}

	public final void stop() {
		stop = true;
	}
}
