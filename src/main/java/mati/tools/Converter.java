package mati.tools;

import com.sun.istack.internal.NotNull;
import mati.simplelogger.SimpleLogger;

import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 *
 * @param <S> Source type
 * @param <T> Target type
 * @param <U> Type of derived class
 *
 * @author Mateusz Paluch
 */
public abstract class Converter<S, T, U extends Converter<S, T, U>> implements Supplier<T> {
	protected static final Logger log = SimpleLogger.getLogger(Converter.class.getSimpleName());

	protected S source;
	protected T result;
	private Thread t;
	protected String error;

	/**
	 * Returns the subclass which extends this abstract superlass
	 * @return
	 */
	protected abstract U getMe();

	/**
	 * Sets the source
	 * @param source
	 * @return
	 */
	@NotNull
	public synchronized final U from(@NotNull S source) {
		this.source = source;
		return getMe();
	}

	public synchronized final U parallel() {
		if (t != null) {
			throw new ConcurrentModificationException();
		}
		t = new Thread(this::parallelRunner);
		return getMe();
	}

	protected void parallelRunner() {

	}

	public synchronized final U process() {
		preProcess();
		onProcess();
		postProcess();
		return getMe();
	}

	protected void preProcess() {

	}

	@NotNull
	protected abstract U onProcess();

	protected void postProcess() {

	}

	@Override
	public synchronized T get() {
		T t = result;
		result = null;
		return t;
	}

	public synchronized final U addTo(Collection<T> output) {
		output.add(result);
		return getMe();
	}

	public synchronized final U withResult(Consumer<T> consumer) {
		consumer.accept(result);
		return getMe();
	}

	public synchronized final void end() {
		onEnd();
		result = null;
	}

	protected void onEnd() {

	}

	public final String getErrorMessage() {
		return error;
	}
}
