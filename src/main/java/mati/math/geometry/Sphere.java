package mati.math.geometry;

import mati.math.vector.Vec3;

public class Sphere {
	public Vec3 center;
	public float radius;

	public Sphere(Vec3 center, float radius) {
		this.center = center;
		this.radius = radius;
	}

}
