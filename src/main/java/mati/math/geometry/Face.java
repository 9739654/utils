package mati.math.geometry;

import mati.math.vector.Vec2;
import mati.math.vector.Vec3;

import java.util.ArrayList;

public abstract class Face {

	public enum FaceComponent {
		VERTEX(2), TEXTURE(3), NORMAL(3);

		private int number;

		private FaceComponent(int minimumRequiredNumber) {
			number = minimumRequiredNumber;
		}

		public final int getNumber() {
			return number;
		}
	}

	protected ArrayList<Vec3> vertices;
	protected ArrayList<Vec3> normals;
	protected ArrayList<Vec2> textureCoords;

	public Face() {
		vertices = new ArrayList<>(getVertexPerFace());
	}

	private final void assureNormals() {
		if (normals == null) {
			normals = new ArrayList<>();
		}
	}

	private final void assureTexCoords() {
		if (textureCoords == null) {
			textureCoords = new ArrayList<>();
		}
	}

	public final void set(int index, Vec3 vertex, Vec2 textureCoord, Vec3 normal) {
		assureNormals();
		assureTexCoords();
		setVertex(index, vertex);
		setNormal(index, normal);
		setTextureCoord(index, textureCoord);
	}

	public final void set(int index, Vec3 vertex, Vec3 normal) {
		assureNormals();
		setVertex(index, vertex);
		setNormal(index, normal);
	}

	public final void set(int index, Vec3 vertex, Vec2 textureCoord) {
		setVertex(index, vertex);
		setTextureCoord(index, textureCoord);
	}

	public final void set(int index, Vec3 vertex) {
		setVertex(index, vertex);
	}

	public final void setVertex(int index, Vec3 vertex) {
		vertices.set(index, vertex);
	}

	public final void addVertex(Vec3 vertex) {
		vertices.add(vertex);
	}

	public final void addNormal(Vec3 normal) {
		normals.add(normal);
	}

	public final void addTexCoord(Vec2 texCoord) {
		textureCoords.add(texCoord);
	}

	public final void setNormal(int index, Vec3 normal) {
		normals.set(index, normal);
	}

	public final void setTextureCoord(int index, Vec2 textureCoord) {
		textureCoords.set(index, textureCoord);
	}

	public abstract int getVertexPerFace();

	public final Vec3 getVertex(int index) {
		return vertices.get(index);
	}

	public final Vec3 getNormal(int index) {
		return normals.get(index);
	}

	public final Vec2 getTexCoord(int index) {
		return textureCoords.get(index);
	}
}
