package mati.math.geometry;

import mati.math.vector.Vec3;

public class Line {
	private Vec3 point;
	private Vec3 direction;

	public Line(Vec3 point, Vec3 direction) {
		this.point = point;
		this.direction = direction;
	}

	public final void setPoint(Vec3 point) {
		this.point.set(point);
	}

	public final void setDirection(Vec3 direction) {
		this.direction.set(direction);
	}
}
