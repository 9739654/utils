package mati.math.geometry;

public class Triangle extends Face {

	@Override
	public final int getVertexPerFace() {
		return 3;
	}
}
