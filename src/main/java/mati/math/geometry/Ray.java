package mati.math.geometry;

import mati.math.vector.Vec3;

public class Ray {
	public Vec3 point;
	public Vec3 direction;

	public final void setPoint(Vec3 point) {
		this.point.set(point);
	}

	public final void setDirection(Vec3 direction) {
		this.direction.set(direction);
	}
}
