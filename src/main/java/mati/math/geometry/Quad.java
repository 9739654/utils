package mati.math.geometry;

import mati.math.vector.Vec3;

public class Quad extends Face {

	@Override
	public final int getVertexPerFace() {
		return 4;
	}
}
