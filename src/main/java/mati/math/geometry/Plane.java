package mati.math.geometry;

import mati.math.vector.Vec3;

/**
 * 'Game Engine Architecture' p. 215
 */
public class Plane {
	public Vec3 point;
	public Vec3 normal;

	public Plane(Vec3 point, Vec3 normal) {
		this.point = point;
		this.normal = normal;
	}

	public final void setPoint(Vec3 point) {
		this.point.set(point);
	}

	public final void setNormal(Vec3 normal) {
		this.normal.set(normal);
	}
}
