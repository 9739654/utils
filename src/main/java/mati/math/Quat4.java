package mati.math;

import mati.math.matrix.Mat4;
import mati.math.vector.Vec;
import mati.math.vector.Vec3;

import java.security.InvalidParameterException;

/**
 * @author Mateusz Paluch
 * @version 0.1
 */
public class Quat4 {
	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;
	public static final int W = 3;


	public float[] data;

	public final float x() {
		return data[X];
	}

	public final float y() {
		return data[Y];
	}

	public final float z() {
		return data[Z];
	}

	public final float w() {
		return data[W];
	}

	public Quat4(float[] axis, @Radians float angle) {
		data = new float[4];
		float sin = (float) Math.sin(angle * 0.5) ;
		for (int i = 0; i < axis.length; i++) {
			data[i] = axis[i] * sin;
		}
		data[W] = (float) Math.cos(angle * 0.5);
	}

	public Quat4(@Radians float... data) {
		this.data = data;
	}

	/**
	 *
	 * @param axis
	 * @param angle angle in radians
	 */
	public Quat4(Vec3 axis, @Radians float angle) {
		this(axis.data, angle);
	}

	public Quat4(String input) {
		parse(input);
	}

	public final void mulSelf(float scalar) {
		for (int i = 0; i < data.length; i++) {
			data[i] *= scalar;
		}
	}

	public final Quat4 mul(float scalar) {
		float[] result = new float[4];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] * scalar;
		}
		return new Quat4(result);
	}

	/**
	 * Grassman product
	 * @param other
	 * @return
	 */
	public final Quat4 mul(Quat4 other) {
		float[] result = new float[4];
		// Ps*Qv
		float[] cross = Vec3.cross(data, other.data);
		for (int i = 0; i < 3; i++) {
			result[i] = data[W] * other.data[i] + other.data[W] * data[i] + cross[i];
		}
		result[3] = data[W] * other.data[W] - Vec.dot(data, other.data, 3);
		return new Quat4(result);
	}

	public final Quat4 conjugate() {
		float[] result = new float[4];
		for (int i = 0; i < 3; i++) {
			result[i] = -data[i];
		}
		result[3] = data[3];
		return new Quat4(result);
	}

	public final Quat4 inverse() {
		float div = 1.0f / squaredMagnitude();
		Quat4 result = conjugate();
		result.mulSelf(div);
		return result;
	}

	public final float magnitude() {
		return (float) Math.sqrt(squaredMagnitude());
	}

	public final float squaredMagnitude() {
		float result = 0;
		for (int i = 0; i < data.length; i++) {
			result += data[i] * data[i];
		}
		return result;
	}

	public final boolean normalised() {
		return squaredMagnitude() == 1;
	}

	public void normalize() {
		float sum = 0;
		for (int i=0; i<3; i++) {
			sum += data[i] * data[i];
		}
		float n = (float)Math.sqrt(sum);
		for (int i=0; i<3; i++) {
			data[i] /= n;
		}
	}

	public final Quat4 lerp(Quat4 other, float beta) {
		float[] result = new float[4];
		//TODO implement 'Game Engine Architecture' p.206
		return new Quat4(result);
	}

	public final Quat4 slerp(Quat4 other, float beta) {
		float[] result = new float[4];
		// TODO implement 'Game Engine Architecture' p.207
		return new Quat4(result);
	}

	public final Mat4 toMat4() {
		float xx = data[X] * data[X];
		float xy = data[X] * data[Y];
		float xz = data[X] * data[Z];
		float xw = data[X] * data[W];
		float yy = data[Y] * data[Y];
		float yz = data[Y] * data[Z];
		float yw = data[Y] * data[W];
		float zz = data[Z] * data[Z];
		float zw = data[Z] * data[W];
		float[] result = new float[] {
				1-2*yy-2*zz,    2*xy + 2*zw,    2*xz-2*yw,      0,
				2*xy-2*zw,      1-2*xx-2*zz,    2*yz+2*xw,      0,
				2*xz+2*yw,      2*yz-2*xw,      1-2*xx-2*yy,    0,
				0,              0,              0,              1
		};
		return new Mat4(result);
	}

	public void parse(String input) {
		String[] strings = input.split(" ");
		if (data == null) {
			data = new float[4];
		}
		for (int i=0; i<4; i++) {
			data[i] = Float.parseFloat(strings[i]);
		}
		if (!normalised()) {
			normalize();
		}
	}
}
