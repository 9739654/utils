package mati.math.matrix;

import mati.math.Quat;
import mati.math.vector.Vec3;

import javax.vecmath.Matrix4f;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Data int this matrix is represented in column-major order
 */
public class Mat4 extends Mat<Mat4> {

	public Mat4() {
		data = new float[16];
	}

	public Mat4(float[] data) {
		if (data.length != 16) {
			throw new IndexOutOfBoundsException("data length != 16");
		}
		this.data = data;
	}

	public Mat4(Mat4 source) {
		data = new float[16];
		System.arraycopy(source.data, 0, data, 0, 16);
	}

	public Mat4(Matrix4f source) {
		data = new float[16];
		fromJavax(source);
	}

	public Mat4(Quat q) {
		double xx = q.x() * q.x();
		double xy = q.x() * q.y();
		double xz = q.x() * q.z();
		double xw = q.x() * q.w();

		double yy = q.y() * q.y();
		double yz = q.y() * q.z();
		double yw = q.y() * q.w();

		double zz = q.z() * q.z();
		double zw = q.z() * q.w();

		data = new float[16];
		this.data[Mat4.m00] = (float)(1 * (1.0 - 2.0 * yy - 2.0 * zz));
		this.data[Mat4.m10] = (float)(1 * 2.0 * (xy + zw));
		this.data[Mat4.m20] = (float)(1 * 2.0 * (xz - yw));
		this.data[Mat4.m01] = (float)(1 * 2.0 * (xy - zw));
		this.data[Mat4.m11] = (float)(1 * (1.0 - 2.0 * xx - 2.0 * zz));
		this.data[Mat4.m21] = (float)(1 * 2.0 * (yz + xw));
		this.data[Mat4.m02] = (float)(1 * 2.0 * (xz + yw));
		this.data[Mat4.m12] = (float)(1 * 2.0 * (yz - xw));
		this.data[Mat4.m22] = (float)(1 * (1.0 - 2.0 * xx - 2.0 * yy));
		this.data[Mat4.m03] = 0;
		this.data[Mat4.m13] = 0;
		this.data[Mat4.m23] = 0;
		this.data[Mat4.m30] = 0;
		this.data[Mat4.m31] = 0;
		this.data[Mat4.m32] = 0;
		this.data[Mat4.m33] = 1;
	}

	public final void loadIdentity() {
		for (int i = 0; i < 16; i++) {
			data[i] = 0;
		}
		data[m00] = 1;
		data[m11] = 1;
		data[m22] = 1;
		data[m33] = 1;
	}

	@Override
	protected final Mat4 getMe() {
		return this;
	}

	@Override
	public void set(Mat4 other) {
		for (int i = 0; i < 16; i++) {
			data[i] = other.data[i];
		}
	}

	/**
	 * Multiplies this matrix by the float scalar
	 *
	 * @param scalar the number to multiply by
	 * @return the result of multiplication
	 */
	@Override
	public final Mat4 mul(float scalar) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] * scalar;
		}
		return new Mat4(result);
	}

	@Override
	public final void mulSelf(Mat4 other) {
		mulM4M4Self(data, other.data);
	}

	/**
	 * Multiplies this matrix by other and  returns the result
	 *
	 * @param other matrix to multiply by
	 * @return result of multiplication
	 */
	@Override
	public final Mat4 mul(Mat4 other) {
		Mat4 target = new Mat4();
		mulM4M4(data, other.data, target.data);
		return target;
	}

	@Override
	public final Mat4 add(Mat4 other) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] + other.data[i];
		}
		return new Mat4(result);
	}

	@Override
	public Mat4 sub(Mat4 other) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] - other.data[i];
		}
		return new Mat4(result);
	}

	public final void translate(Vec3 translation) {
		translate(translation.data);
	}

	public final void translate(float... translation) {
		data[m00] += data[m03] * translation[Vec3.X];
		data[m01] += data[m03] * translation[Vec3.Y];
		data[m02] += data[m03] * translation[Vec3.Z];

		data[m10] += data[m13] * translation[Vec3.X];
		data[m11] += data[m13] * translation[Vec3.Y];
		data[m12] += data[m13] * translation[Vec3.Z];

		data[m20] += data[m23] * translation[Vec3.X];
		data[m21] += data[m23] * translation[Vec3.Y];
		data[m22] += data[m23] * translation[Vec3.Z];

		data[m30] += data[m33] * translation[Vec3.X];
		data[m31] += data[m33] * translation[Vec3.Y];
		data[m32] += data[m33] * translation[Vec3.Z];
	}

	public final void scale(float scale) {
		data[m00] *= scale;
		data[m01] *= scale;
		data[m02] *= scale;

		data[m10] *= scale;
		data[m11] *= scale;
		data[m12] *= scale;

		data[m20] *= scale;
		data[m21] *= scale;
		data[m22] *= scale;

		data[m30] *= scale;
		data[m31] *= scale;
		data[m32] *= scale;
	}

	/**
	 * Sets the translations as in the given vector
	 *
	 * @param translation translation amount
	 */
	public final void setTranslation(Vec3 translation) {
		data[m30] = translation.x();
		data[m31] = translation.y();
		data[m32] = translation.z();
	}

	public final void setRotation(Quat rotation) {
		rotation.toRotationMatrix(this);
	}

	public final void setScale(Vec3 scale) {
		setScale(scale.x(), scale.y(), scale.z());
	}

	public final void setScale(float x, float y, float z) {
		Vec3 temp = new Vec3(data[m00], data[m01], data[m02]);
		temp.normalizeSelf();
		temp.mulSelf(x);
		data[m00] = temp.x();
		data[m01] = temp.y();
		data[m02] = temp.z();

		temp.set(data[m10], data[m11], data[m12]);
		temp.normalizeSelf();
		temp.mulSelf(y);
		data[m10] = temp.x();
		data[m11] = temp.y();
		data[m12] = temp.z();

		temp.set(data[m20], data[m21], data[m22]);
		temp.normalizeSelf();
		temp.mulSelf(z);
		data[m20] = temp.x();
		data[m21] = temp.y();
		data[m22] = temp.z();
	}

	public final Vec3 getTranslation() {
		return new Vec3(data[m30], data[m31], data[m32]);
	}

	public final void getTranslation(Vec3 target) {
		target.set(data[m30], data[m31], data[m32]);
	}

	public final Quat getRotation() {
		Quat quat = new Quat();
		quat.setFromMatrix(this);
		return quat;
	}

	public final void getScale(Vec3 result) {
		float scaleX = (float) Math.sqrt(data[m00] * data[m00] + data[m01] * data[m01] + data[m02] * data[m02]);
		float scaleY = (float) Math.sqrt(data[m10] * data[m10] + data[m11] * data[m11] + data[m12] * data[m12]);
		float scaleZ = (float) Math.sqrt(data[m20] * data[m20] + data[m21] * data[m21] + data[m22] * data[m22]);
		result.set(scaleX, scaleY, scaleZ);
	}

	public final Vec3 getScale() {
		Vec3 result = new Vec3();
		getScale(result);
		return result;
	}

	/////

	public static Mat4 makeTranslation(Vec3 source) {
		float[] result = new float[] {
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				source.data[Vec3.X], source.data[Vec3.Y], source.data[Vec3.Z], 1
		};
		return new Mat4(result);
	}

	/**
	 *
	 * @param angle radians
	 * @return
	 */
	public static Mat4 makeRotationX(float angle) {
		float cosAngle = (float) Math.cos(angle);
		float[] result = new float[] {
				1, 0,                       0,                      0,
				0, cosAngle,                (float)Math.sin(angle), 0,
				0, (float)-Math.sin(angle), cosAngle, 0,
				0, 0,                       0,                      1

		};
		return new Mat4(result);
	}

	/**
	 *
	 * @param angle radians
	 * @return
	 */
	public static Mat4 makeRotationY(float angle) {
		float cosAngle = (float) Math.cos(angle);
		float[] result = new float[]{
				cosAngle,    0, (float) -Math.sin(angle),    0,
				0,                          1, 0,                           0,
				(float) Math.sin(angle),    0, cosAngle,     0,
				0,                          0, 0,                           1

		};
		return new Mat4(result);
	}

	/**
	 *
	 * @param angle radians
	 * @return
	 */
	public static Mat4 makeRotationZ(float angle) {
		float cosAngle = (float) Math.cos(angle);
		float[] result = new float[]{
				cosAngle,                   (float) Math.sin(angle),    0, 0,
				(float)-Math.sin(angle),    cosAngle,                   0, 0,
				0,                          0,                          1, 0,
				0,                          0,                          0, 1

		};
		return new Mat4(result);
	}

	public static Mat4 makeScale(float x, float y, float z) {
		float[] result = new float[] {
				x, 0, 0, 0,
				0, y, 0, 0,
				0, 0, z, 0,
				0, 0, 0, 1
		};
		return new Mat4(result);
	}

	public static Mat4 makeScale(float scale) {
		return makeScale(scale, scale, scale);
	}

	/////

	/**
	 * Gets right vector
	 * @return
	 */
	public final Vec3 right() {
		return new Vec3(data[m00], data[m01], data[m02]);
	}

	/**
	 * Gets front vector
	 * @return
	 */
	public final Vec3 front() {
		return new Vec3(data[m10], data[m11], data[m12]);
	}

	/**
	 * Gets up vector
	 * @return
	 */
	public final Vec3 up() {
		return new Vec3(data[m20], data[m21], data[m22]);
	}

	/////

	public final void identity() {
					data[m01] = data[m02] = data[m03] = 0;
		data[m10] =             data[m12] = data[m13] = 0;
		data[m20] = data[m21] =             data[m23] = 0;
		data[m30] = data[m31] = data[m32] =             0;
		data[m00] = data[m11] = data[m22] = data[m33] = 1;
	}

	public final void set(int x, int y, float element) {
		data[at(x, y)] = element;
	}

	public final float get(int x, int y) {
		return data[at(x, y)];
	}

	private static final int at(int x, int y) {
		return y * 4 + x;
	}

	//////

	/**
	 * c = a * b
	 * @param a
	 * @param b
	 * @param c
	 */
	private static void mulM4M4(float[] a, float[] b, float[] c) {
		// first row
		c[m00] = a[m00] * b[m00] + a[m01] * b[m10] + a[m02] * b[m20] + a[m03] * b[m30];
		c[m01] = a[m00] * b[m01] + a[m01] * b[m11] + a[m02] * b[m21] + a[m03] * b[m31];
		c[m02] = a[m00] * b[m02] + a[m01] * b[m12] + a[m02] * b[m22] + a[m03] * b[m32];
		c[m03] = a[m00] * b[m03] + a[m01] * b[m13] + a[m02] * b[m23] + a[m03] * b[m33];

		// second row
		c[m10] = a[m10] * b[m00] + a[m11] * b[m10] + a[m12] * b[m20] + a[m13] * b[m30];
		c[m11] = a[m10] * b[m01] + a[m11] * b[m11] + a[m12] * b[m21] + a[m13] * b[m31];
		c[m12] = a[m10] * b[m02] + a[m11] * b[m12] + a[m12] * b[m22] + a[m13] * b[m32];
		c[m13] = a[m10] * b[m03] + a[m11] * b[m13] + a[m12] * b[m23] + a[m13] * b[m33];

		// third row
		c[m20] = a[m20] * b[m00] + a[m21] * b[m10] + a[m22] * b[m20] + a[m23] * b[m30];
		c[m21] = a[m20] * b[m01] + a[m21] * b[m11] + a[m22] * b[m21] + a[m23] * b[m31];
		c[m22] = a[m20] * b[m02] + a[m21] * b[m12] + a[m22] * b[m22] + a[m23] * b[m32];
		c[m23] = a[m20] * b[m03] + a[m21] * b[m13] + a[m22] * b[m23] + a[m23] * b[m33];

		// fourth row
		c[m30] = a[m30] * b[m00] + a[m31] * b[m10] + a[m32] * b[m20] + a[m33] * b[m30];
		c[m31] = a[m30] * b[m01] + a[m31] * b[m11] + a[m32] * b[m21] + a[m33] * b[m31];
		c[m32] = a[m30] * b[m02] + a[m31] * b[m12] + a[m32] * b[m22] + a[m33] * b[m32];
		c[m33] = a[m30] * b[m03] + a[m31] * b[m13] + a[m32] * b[m23] + a[m33] * b[m33];
	}

	/**
	 * a = a * b
	 */
	private static final void mulM4M4Self(float[] a, float[] b) {
		// first row
		float c0 = a[m00] * b[m00] + a[m01] * b[m10] + a[m02] * b[m20] + a[m03] * b[m30];
		float c1 = a[m00] * b[m01] + a[m01] * b[m11] + a[m02] * b[m21] + a[m03] * b[m31];
		float c2 = a[m00] * b[m02] + a[m01] * b[m12] + a[m02] * b[m22] + a[m03] * b[m32];
		float c3 = a[m00] * b[m03] + a[m01] * b[m13] + a[m02] * b[m23] + a[m03] * b[m33];
		a[m00] = c0;
		a[m01] = c1;
		a[m02] = c2;
		a[m03] = c3;

		// second row
		c0 = a[m10] * b[m00] + a[m11] * b[m10] + a[m12] * b[m20] + a[m13] * b[m30];
		c1 = a[m10] * b[m01] + a[m11] * b[m11] + a[m12] * b[m21] + a[m13] * b[m31];
		c2 = a[m10] * b[m02] + a[m11] * b[m12] + a[m12] * b[m22] + a[m13] * b[m32];
		c3 = a[m10] * b[m03] + a[m11] * b[m13] + a[m12] * b[m23] + a[m13] * b[m33];
		a[m10] = c0;
		a[m11] = c1;
		a[m12] = c2;
		a[m13] = c3;

		// third row
		c0 = a[m20] * b[m00] + a[m21] * b[m10] + a[m22] * b[m20] + a[m23] * b[m30];
		c1 = a[m20] * b[m01] + a[m21] * b[m11] + a[m22] * b[m21] + a[m23] * b[m31];
		c2 = a[m20] * b[m02] + a[m21] * b[m12] + a[m22] * b[m22] + a[m23] * b[m32];
		c3 = a[m20] * b[m03] + a[m21] * b[m13] + a[m22] * b[m23] + a[m23] * b[m33];
		a[m20] = c0;
		a[m21] = c1;
		a[m22] = c2;
		a[m23] = c3;

		// fourth row
		c0 = a[m30] * b[m00] + a[m31] * b[m10] + a[m32] * b[m20] + a[m33] * b[m30];
		c1 = a[m30] * b[m01] + a[m31] * b[m11] + a[m32] * b[m21] + a[m33] * b[m31];
		c2 = a[m30] * b[m02] + a[m31] * b[m12] + a[m32] * b[m22] + a[m33] * b[m32];
		c3 = a[m30] * b[m03] + a[m31] * b[m13] + a[m32] * b[m23] + a[m33] * b[m33];
		a[m30] = c0;
		a[m31] = c1;
		a[m32] = c2;
		a[m33] = c3;
	}

	/**
	 * m[col] = m * v
	 * @param m
	 * @param col
	 * @param v
	 */
	@Deprecated
	private static final void mulM4V4Self(float[] m, int col, float[] v) {
		// TODO check
		m[col + 0] = m[0] * v[0] + m[1] * v[1] + m[2] * v[2] + m[3] * v[3];
		m[col + 4] = m[4] * v[0] + m[5] * v[1] + m[6] * v[2] + m[7] * v[3];
		m[col + 8] = m[8] * v[0] + m[9] * v[1] + m[10] * v[2] + m[11] * v[3];
		m[col + 12] = m[12] * v[0] + m[13] * v[1] + m[14] * v[2] + m[15] * v[3];
	}

	/**
	 * v = m*axis(a)
	 * @param m
	 * @param a 0=X, 1=Y, 2=Z
	 * @param v
	 */
	@Deprecated
	private static final void mulM4Axis(float[] m, int a, float[] v) {
		int s = 1-a, t = 2-a, u = 3-a;
		int x = t*a;
		int y = s*a;
		int z = s*t;
		v[0] = m[0] * x + m[1] * y + m[2] * z;
		v[1] = m[4] * x + m[5] * y + m[6] * z;
		v[2] = m[8] * x + m[9] * y + m[10]* z;
		v[3] = m[12]* x + m[13]* y + m[14]* z;
	}

	/**
	 * c = a * b
	 * @param a
	 * @param b
	 * @param c
	 */
	@Deprecated
	private static final void mulM4V4(float[] a, float[] b, float[] c) {
		// first row
		c[0] = a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];

		// second row
		c[1] = a[4] * b[0] + a[5] * b[1] + a[6] * b[2] + a[7] * b[3];

		// third row
		c[2] = a[8] * b[0] + a[9] * b[1] + a[10] * b[2] + a[11] * b[3];

		// fourth row
		c[3] = a[12] * b[0] + a[13] * b[1] + a[14] * b[2] + a[15] * b[3];
	}

	/**
	 * m.rot = q
	 * @param q
	 * @param m
	 */
	@Deprecated
	private static final void convQ4toM4(float[] q, float[] m) {
		double xx = q[0] * q[0];
		double xy = q[0] * q[0];
		double xz = q[0] * q[2];
		double xw = q[0] * q[3];

		double yy = q[1] * q[1];
		double yz = q[1] * q[2];
		double yw = q[1] * q[3];

		double zz = q[2] * q[2];
		double zw = q[2] * q[3];

		m[m00] = (float)(1 - 2 * yy - 2 * zz);
		m[m01] = (float)(2 * xy - 2 * zw);
		m[m02] = (float)(2 * xz + 2 * yw);
//		m[m03] = (float)(0);
		m[m10] = (float)(2 * xy + 2 * zw);
		m[m11] = (float)(1 - 2 * xx - 2 * zz);
		m[m12] = (float)(2 * yz - 2 * xw);
//		m[m13] = (float)(0);
		m[m20] = (float)(2 * xz - 2 * yw);
		m[m21] = (float)(2 * yz + 2 * xw);
		m[m22] = (float)(1 - 2 * xx - 2 * yy);
//		m[m23] = 0;
//		m[m30] = 0;
//		m[m31] = 0;
//		m[m32] = 0;
//		m[m33] = 1;
	}
	
	public static final Mat4 makeIdentity() {
		float[] data = new float[] {
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
		};
		return new Mat4(data);
	}

	public final FloatBuffer makeDirectBuffer() {
		ByteBuffer bb = ByteBuffer.allocateDirect(16 * 4);
		bb.order(ByteOrder.nativeOrder());
		FloatBuffer fb = bb.asFloatBuffer();
		fb.put(data).flip();
		return fb;
	}

	public final boolean isIdentity() {
		return  data[m00] == 1 && data[m11] ==  1 && data[m22] == 1 && data[m33] == 1
				&& data[m01] == 0 && data[m02] == 0 && data[m03] == 0
				&& data[m10] == 0 && data[m12] == 0 && data[m13] == 0
				&& data[m20] == 0 && data[m21] == 0 && data[m23] == 0
				&& data[m30] == 0 && data[m31] == 0 && data[m32] == 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Mat4)) {
			return false;
		}
		Mat4 m2 = (Mat4) obj;

		return identical(m2);
	}

	/**
	 * Returns true if matrices are the same
	 * @param other
	 * @return
	 */
	public boolean identical(Mat4 other) {
		for (int i = 0; i < 16; i++) {
			if (data[i] != other.data[i]) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns true if this matrix is the other matrix. Compares references.
	 * @param other
	 * @return
	 */
	public boolean is(Mat4 other) {
		return this == other;
	}

	@Override
	public String toString() {
		String res = "";

		for (int i=0; i<16; i++) {
			res += data[i] + ", ";
			if ((i+1) % 4 == 0) {
				res += '\n';
			}
		}

		return res;
	}

	public final Matrix4f toJavax() {
		Matrix4f m = new Matrix4f();
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				m.setElement(i, j, data[at(i, j)]);
			}
		}
		return m;
	}

	public final void fromJavax(Matrix4f source) {
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				data[at(i, j)] = source.getElement(i, j);
			}
		}
	}

}
