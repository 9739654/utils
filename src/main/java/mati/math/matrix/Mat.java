package mati.math.matrix;

public abstract class Mat<T extends Mat<T>> {
	public float[] data;

	public static final int m00 = 0;
	public static final int m01 = 1;
	public static final int m02 = 2;
	public static final int m03 = 3;

	public static final int m10 = 4;
	public static final int m11 = 5;
	public static final int m12 = 6;
	public static final int m13 = 7;

	public static final int m20 = 8;
	public static final int m21 = 9;
	public static final int m22 = 10;
	public static final int m23 = 11;

	public static final int m30 = 12;
	public static final int m31 = 13;
	public static final int m32 = 14;
	public static final int m33 = 15;

	protected abstract T getMe();

	/**
	 * Sets values of this matrix to the same as other.
	 * @param other
	 */
	public abstract void set(T other);

	/**
	 * Multiplies this matrix by the float scalar
	 *
	 * @param scalar the number to multiply by
	 * @return the result of multiplication
	 */
	public final void mulSelf(float scalar) {
		for (int i = 0; i < data.length; i++) {
			data[i] *= scalar;
		}
	}

	public abstract T mul(float scalar);

	public abstract void mulSelf(T other);

	public abstract T mul(T other);

	public final void addSelf(T other) {
		for (int i = 0; i < data.length; i++) {
			data[i] += other.data[i];
		}
	}

	public abstract T add(T other);

	public final void subSelf(T other) {
		for (int i = 0; i < data.length; i++) {
			data[i] -= other.data[i];
		}
	}

	public abstract T sub(T other);
}
