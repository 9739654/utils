package mati.math.vector;

import mati.math.matrix.Mat;
import mati.math.matrix.Mat4;

import java.security.InvalidParameterException;

/**
 * Created by mati on 29.04.15.
 */
public class Vec4 extends Vec<Vec4> {

	public static final int ORIENTATION = 0;
	public static final int POSITION = 1;
	public static final int Z = 2;
	public static final int W = 3;

	public static final Vec4 axisX = new Vec4(1, 0, 0, ORIENTATION);
	public static final Vec4 axisY = new Vec4(0, 1, 0, ORIENTATION);
	public static final Vec4 axisZ = new Vec4(0, 0, 1, ORIENTATION);

	public Vec4() {
		data = new float[4];
	}

	public Vec4(float... data) {
		if (data.length != 4) {
			throw new InvalidParameterException();
		}
		this.data = data;
	}

	public Vec4(float[] data, float w) {
		this(data[0], data[1], data[2], w);
	}

	public Vec4(Vec4 source) {
		data = new float[4];
		for (int i=0; i<4; i++) {
			data[i] = source.data[i];
		}
	}

	public Vec4(Vec3 source, float w) {
		data = new float[4];
		for (int i=0; i<3; i++) {
			data[i] = source.data[i];
		}
		data[3] = w;
	}

	public Vec4(String input) {
		parse(input);
	}

	public Vec4(String input, float w) {
		data = new float[4];
		parse(input, 3);
		data[3] = w;
	}

	public final float z() {
		return data[Z];
	}

	public final float w() {
		return data[W];
	}

	protected final Vec4 getMe() {
		return this;
	}

	@Override
	public void set(Vec4 other) {
		data[0] = other.data[0];
		data[1] = other.data[1];
		data[2] = other.data[2];
		data[3] = other.data[3];
	}

	@Override
	public final Vec4 mul(float scalar) {
		float[] result = new float[4];
		for (int i=0; i<4; i++) {
			result[i] = data[i] * scalar;
		}
		return new Vec4(result);
	}

	public final Vec4 mul(Mat4 other) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] * other.data[Mat.m00 + i]
					+ data[i] * other.data[Mat.m10 + i]
					+ data[i] * other.data[Mat.m20 + i]
					+ data[i] * other.data[Mat.m30 + i];
		}
		return new Vec4(result);
	}

	@Override
	public Vec4 add(Vec4 other) {
		float[] result = new float[4];
		for (int i=0; i<4; i++) {
			result[i] = data[i] + other.data[i];
		}
		return new Vec4(result);
	}

	@Override
	public Vec4 sub(Vec4 other) {
		float[] result = new float[data.length];
		for (int i=0; i<data.length; i++) {
			result[i] = data[i] - other.data[i];
		}
		return new Vec4(result);
	}

	@Override
	public Vec4 normalize() {
		float oneOverMagnitude = 1.0f / magnitude();
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] * oneOverMagnitude;
		}
		return new Vec4(result);
	}

	public final Vec4 lerp(Vec4 other, float beta) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = (data[i] * (1 - beta)) + (other.data[i] * beta);
		}
		return new Vec4(result);
	}

	@Override
	public void interpolate(Vec4 v1, Vec4 v2, float delta) {
		//TODO
	}

	@Override
	public void parse(String input) {
		parse(input, 4);
	}

}
