package mati.math.vector;

import com.sun.istack.internal.NotNull;
import mati.math.Quat;

public class Vec3 extends Vec<Vec3> {

	public static final int Z = 2;

	public static final Vec3 ZERO = new Vec3(0, 0, 0);
	public static final Vec3 ONE = new Vec3(1, 1, 1);
	public static final Vec3 axisX = new Vec3(1, 0, 0);
	public static final Vec3 axisY = new Vec3(0, 1, 0);
	public static final Vec3 axisZ = new Vec3(0, 0, 1);

	public Vec3() {
		data = new float[] { 0, 0, 0 };
	}

	public Vec3(float... data) {
		this.data = data;
	}

	public Vec3(Vec3 v) {
		this.data = new float[] { v.data[0], v.data[1], v.data[2] };

	}

	public Vec3(String input) {
		parse(input);
	}

	public final float z() {
		return data[Z];
	}

	@Override
	protected final Vec3 getMe() {
		return this;
	}

	public final void set(float x, float y, float z) {
		data[X] = x;
		data[Y] = y;
		data[Z] = z;
	}

	@Override
	public final void set(Vec3 other) {
		for (int i = 0; i < data.length; i++) {
			this.data[i] = other.data[i];
		}
	}

	@Override
	public final Vec3 mul(float scalar) {
		float[] result = new float[data.length];
		for (int i=0; i<data.length; i++) {
			result[i] = data[i] * scalar;
		}
		return new Vec3(result);
	}

	@Override
	public Vec3 add(Vec3 other) {
		float[] result = new float[data.length];
		for (int i=0; i<data.length; i++) {
			result[i] = data[i] + other.data[i];
		}
		return new Vec3(result);
	}

	@Override
	public Vec3 sub(Vec3 other) {
		float[] result = new float[data.length];
		for (int i=0; i<data.length; i++) {
			result[i] = data[i] - other.data[i];
		}
		return new Vec3(result);
	}

	public Vec3 normalize() {
		float[] result = new float[data.length];
		float oneOverMagnitude = 1.0f / magnitude();
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i] * oneOverMagnitude;
		}
		return new Vec3(result);
	}

	/**
	 * Calculates the cross product of this and other and stores the result in this. The cross product is only defined
	 * in three dimensions. 'Game Engine Architecture' p. 176
	 * @param other
	 */
	public final void crossSelf(Vec3 other) {
		float x = data[Y] * other.data[Z] - data[Z] * other.data[Y];
		float y = data[Z] * other.data[X] - data[X] * other.data[Z];
		float z = data[X] * other.data[Y] - data[Y] * other.data[X];
		data[X] = x;
		data[Y] = y;
		data[Z] = z;
	}

	/**
	 * Calculates the cross product of this adn other and returns the result. The cross product is only defined in three
	 * dimensions. 'Game Engine Architecture' p. 176
	 * @param other
	 * @return this x other
	 */
	public final Vec3 cross(Vec3 other) {
		float a = data[Y] * other.data[Z] - data[Z] * other.data[Y];
		float b = data[Z] * other.data[X] - data[X] * other.data[Z];
		float c = data[X] * other.data[Y] - data[Y] * other.data[X];
		return new Vec3(a, b, c);
	}

	public final float cosAngle(Vec3 other) {
		return this.dot(other) / (this.magnitude() * other.magnitude());
	}

	public static final Vec3 normalize(float x, float y, float z) {
		float oneOverMagnitude = 1.0f / magnitude(x, y, z);
		return new Vec3(x * oneOverMagnitude, y * oneOverMagnitude, z * oneOverMagnitude);
	}

	public static final float magnitude(float x, float y, float z) {
		float result = x * x + y * y + z * z;
		return (float) Math.sqrt(result);
	}

	public static float[] cross(float[] a, float[] b) {
		float[] result = new float[3];
		result[0] = a[Y] * b[Z] - a[Z] * b[Y];
		result[1] = a[Z] * b[X] - a[X] * b[Z];
		result[2] = a[X] * b[Y] - a[Y] * b[X];
		return result;
	}

	public final Vec3 lerp(Vec3 other, float beta) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = (data[i] * (1 - beta)) + (other.data[i] * beta);
		}
		return new Vec3(result);
	}

	public final Vec3 rotate(Quat rotation) {
		// TODO rotating vector 'Game Engine Architecture' p. 203
		return null;
	}

	public final Vec4 toVec4() {
		return new Vec4(data[0], data[1], data[2], 1);
	}

	public static Vec3 parse(@NotNull String input, @NotNull String regex) {
		String[] strings = input.split(regex);
		return new Vec3(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]), Float.parseFloat(strings[2]));
	}

	@Override
	public final void interpolate(Vec3 trans1, Vec3 trans2, float delta) {
		//TODO
	}

	@Override
	public void parse(String input) {
		parse(input, 3);
	}

	@Override
	public String toString() {
		return "Vec3 {" + data[X] + ", " + data[Y] + ", " + data[Z] + "}";
	}
}
