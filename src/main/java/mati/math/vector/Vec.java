package mati.math.vector;

import com.sun.istack.internal.NotNull;
import mati.math.geometry.Line;

public abstract class Vec<T extends Vec<T>> {
	public static final int X = 0;
	public static final int Y = 1;

	public static final int YAW = 0;
	public static final int PITCH = 2;
	public static final int ROLL = 1;

	/**
	 * Column-main
	 */
	public float[] data;

	{
		data = null;
	}

	protected abstract T getMe();

	public final float x() {
		return data[X];
	}

	public final float y() {
		return data[Y];
	}

	public abstract void set(T other);

	/**
	 * multiplies this by the given scalar and stores the result in this
	 *
	 * @param scalar
	 */
	public final void mulSelf(float scalar) {
		for (int i = 0; i < data.length; i++) {
			data[i] *= scalar;
		}
	}

	/**
	 * multiplies this by the given scalar and returns the multiplication result. This remains unchanged.
	 *
	 * @param scalar
	 * @return result of multiplication this*scalar
	 */
	public abstract T mul(float scalar);

	/**
	 * Adds other to this and stores the result in this
	 *
	 * @param other
	 */
	public final void addSelf(T other) {
		for (int i = 0; i < data.length; i++) {
			data[i] += other.data[i];
		}
	}

	/**
	 * Adds other to this and returns the result. This remains unchanged.
	 *
	 * @param other
	 * @return this + other
	 */
	public abstract T add(T other);

	/**
	 * this = this - other
	 *
	 * @param other
	 */
	public final void subSelf(T other) {
		for (int i = 0; i < data.length; i++) {
			data[i] -= other.data[i];
		}
	}

	/**
	 * Substracts other from this and returns the result
	 *
	 * @param other
	 * @return this - other
	 */
	public abstract T sub(T other);

	/**
	 * Calculates the length of this
	 *
	 * @return
	 */
	public final float magnitude() {
		float result = 0;
		for (int i = 0; i < data.length; i++) {
			result += data[i] * data[i];
		}
		return (float) Math.sqrt(result);
	}

	public final float squaredMagnitude() {
		return dot(getMe());
	}

	public final void normalizeSelf() {
		float oneOverMagnitude = 1.0f / magnitude();
		for (int i = 0; i < data.length; i++) {
			data[i] *= oneOverMagnitude;
		}
	}

	public abstract T normalize();

	/**
	 * Returns the dot product of this * other.
	 * |this| * |other| * cos(fi)
	 *
	 * @param other
	 * @return
	 */
	public final float dot(T other) {
		return dot(data, other.data, data.length);
	}

	public static float dot(float[] a, float[] b, int length) {
		float result = 0;
		for (int i = 0; i < length; i++) {
			result += a[i] * b[i];
		}
		return result;
	}

	public final float projectOnto(Line other) {
		// TODO implement 'Game Engine Architecture' p. 174
		return 0;
	}

	/**
	 * Returns a vector that lies beta percentage of the way along the line segment from point a to point b.

	 * @param other
	 * @param beta
	 * @return
	 */
	public abstract T lerp(T other, float beta);

	public abstract void interpolate(T v1, T v2, float delta);

	public abstract void parse(String input);

	protected void parse(@NotNull String input, int size, @NotNull String regex) {
		String[] strings = input.split(regex);
		if (data == null) {
			data = new float[size];
		}
		for (int i = 0; i < size; i++) {
			data[i] = Float.parseFloat(strings[i]);
		}
	}

	protected void parse(@NotNull String input, int size) {
		parse(input, size, " ");
	}
}
