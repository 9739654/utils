package mati.math.vector;

import com.sun.istack.internal.NotNull;

public class Vec2 extends Vec<Vec2> {

	public static final int U = 0;
	public static final int V = 1;

	public Vec2(float... data) {
		this.data = data;
	}

	public final float u() {
		return data[U];
	}

	public final float v() {
		return data[V];
	}

	@Override
	protected final Vec2 getMe() {
		return this;
	}

	@Override
	public void set(Vec2 other) {
		data[0] = other.data[0];
		data[1] = other.data[1];
	}

	@Override
	public final Vec2 mul(float scalar) {
		return new Vec2(data[X] * scalar, data[Y] * scalar);
	}

	@Override
	public final Vec2 add(Vec2 other) {
		return new Vec2(data[X] + other.data[X], data[Y] + other.data[Y]);
	}

	@Override
	public final Vec2 sub(Vec2 other) {
		return new Vec2(data[X] - other.data[X], data[Y] - other.data[Y]);
	}

	@Override
	public final Vec2 normalize() {
		float length = data[X] * data[X] + data[Y] * data[Y];
		length = (float) (1.0 / Math.sqrt(length));
		return new Vec2(data[X] * length, data[Y] * length);
	}

	@Override
	public final Vec2 lerp(Vec2 other, float beta) {
		float[] result = new float[data.length];
		for (int i = 0; i < data.length; i++) {
			result[i] = (data[i] * (1 - beta)) + (other.data[i] * beta);
		}
		return new Vec2(result);
	}

	@Override
	public void interpolate(Vec2 v1, Vec2 v2, float delta) {
		//TODO
	}

	public static Vec2 parse(@NotNull String input, @NotNull String regex) {
		String[] parts = input.split(regex);
		return new Vec2(Float.parseFloat(parts[0]), Float.parseFloat(parts[1]));
	}

	@Override
	public final void parse(String input) {
		parse(input, 2);
	}
}
