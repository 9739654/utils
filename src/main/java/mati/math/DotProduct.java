package mati.math;

public class DotProduct {

	/**
	 * Dot product of unit vectors equals 1 if they are colinear. Angle between vectors equals 0 deg.
	 * @param dot
	 * @return
	 */
	public static final boolean colinear(float dot) {
		return dot == 1 || dot == -1;
	}

	/**
	 * Dot product of unit vectors equals == 0 if they are perpendicular. Angle between vectors equals 90 deg.
	 * @param dot
	 * @return
	 */
	public static final boolean perpendicular(float dot) {
		return dot == 0;
	}

	/**
	 * Dot product of unit vectors is greater than 0 if the angle between them is less than 90 deg.
	 * @param dot
	 * @return
	 */
	public static final boolean sameDirections(float dot) {
		return dot > 0;
	}

	/**
	 * Dot product of unit vectors is less than 0 if the angle between them is greater than 90 deg.
	 * @param dot
	 * @return
	 */
	public static final boolean oppositeDirections(float dot) {
		return dot < 0;
	}

}
