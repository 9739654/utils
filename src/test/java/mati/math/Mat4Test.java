package mati.math;

import mati.math.matrix.Mat4;
import mati.math.vector.Vec4;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by mati on 02.05.15.
 */
public class Mat4Test {

	@Test
	public void testMakeIdentity() {
		Mat4 ide = Mat4.makeIdentity();

		assertTrue("Make identity", ide.isIdentity());
	}

	@Test
	public void testIdentityMultiplication() {
		Mat4 m1 = Mat4.makeIdentity();
		Mat4 m2 = Mat4.makeIdentity();
		Mat4 result = m2.mul(m2);

		assertTrue("IDentiyty * Identity == Identity", result.isIdentity());
	}

	@Test
	public void testCustomMultiplication() {
		Mat4 m1 = Mat4.makeIdentity();
		Mat4 m2 = new Mat4(new float[] {
				1, 2, 3, 4,
				0, 1, 2, 0,
				3, -2, -1, 0,
				1, 1, 1, 1
		});

		Mat4 result = m1.mul(m2);
		assertTrue("identiyty * x = x", result.equals(m2));
	}

	@Test
	public void testMultiplication() {
		Mat4 m1 = new Mat4(new float[] {
				1,  2,  3,  4,
				2,  3,  4,  5,
				3,  4,  5,  6,
				4,  5,  6,  7
		});
		Mat4 m2 = new Mat4(new float[] {
				4,  3,  2,  1,
				3,  2,  1,  0,
				2,  1,  0,  -1,
				1,  0,  -1, -2
		});

		Mat4 result = m1.mul(m2);
		Mat4 test = new Mat4(new float[] {
				20, 10, 0, -10,
				30, 16, 2, -12,
				40, 22, 4, -14,
				50, 28, 6, -16
		});

		System.out.println("result:");
		System.out.println(result);
		System.out.println("test:");
		System.out.println(test);
		assertTrue("Multiplication works", result.equals(test));
	}

	private static boolean in(float val, float expected, float margin) {
		return expected - margin < val && val < expected + margin;
	}
}
